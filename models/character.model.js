class Character {
  constructor(id, name, imageSrc) {
    this._id = id;
    this._name = name;
    this._imageSrc = imageSrc;
  }

  get id() {
    return this._id;
  }

  set id(value) {
    this._id = value;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get imageSrc() {
    return this._imageSrc;
  }

  set imageSrc(value) {
    this._imageSrc = value;
  }
}

module.exports = Character;