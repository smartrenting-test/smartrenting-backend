var express = require('express');
var router = express.Router();
const character = require('./character.controller');

/* GET users listing. */
router.get('/offset/:offset', character.getCharacters);

module.exports = router;
