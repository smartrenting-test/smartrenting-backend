const characters = require('../../services/marvelApi.service');

module.exports = {
  getCharacters: async ({params}, res) => {
    const offset = params.offset;
    characters.getCharacters(offset).then((data) => {
      return res.json(data);
    });
  }
};
