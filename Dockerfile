FROM node:10.6.0

COPY . /app

WORKDIR /app

RUN npm install
RUN npm install -g nodemon

EXPOSE 3000

CMD npm run start-dev