module.exports = {
  API: {
    API_KEY_PUB: process.env.API_KEY_PUB,
    API_KEY_PRIV: process.env.API_KEY_PRIV
  }
}