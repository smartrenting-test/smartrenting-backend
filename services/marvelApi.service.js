const https = require('https');
const md5 = require('md5');
const Character = require('../models/character.model');
const config = require('../config/config');

const host = 'https://gateway.marvel.com:443/v1/public/';
const apiKey = config.API.API_KEY_PUB;
const privateKey = config.API.API_KEY_PRIV;


function getCharacters(offset) {
  let ts = new Date().getTime();

  let stringToHash = ts + privateKey + apiKey;
  let hash = md5(stringToHash);
  let limit = 20;

  const url = `${host}characters?ts=${ts}&apikey=${apiKey}&hash=${hash}&limit=${limit}&offset=${offset * limit}`;
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      let body = '';

      res.on('data', (d) => {
        body += d;
      });

      res.on('end', () => {
        const response = JSON.parse(body);
        let characters = [];

        response.data.results.forEach((el) => {
          characters.push(new Character(el.id, el.name, `${el.thumbnail.path}.${el.thumbnail.extension}`));
        });
        resolve(characters);
      });

      res.on('error', (error) => {
        reject(error);
      });
    });
  });
}

module.exports = {
  getCharacters,
};